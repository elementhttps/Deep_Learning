'''
==================================
Central limit theorem 
==================================
- graficki prikaz CLT  
- teorija


------------------------------------------------------------------------
Teorija 
------------------------------------------------------------------------

central limit theorem states that the distribution of the sum (or average) 
of a large number of independent, 
identically distributed variables 
will be approximately normal


'''
from matplotlib import pyplot as plt
import numpy as np 


# generate random data 
rand_min = 1
rand_max = 6
rand_size = 1000000

population = np.random.uniform(rand_min,rand_max,rand_size)

#sample size 
sample_size = 10
mean = []



broj_samplova  = [20,50,100,500,1000,10000]



j = 0
while True:   
    j+=1
    # print('parametri',j) 
    plt.subplot(2,3,j)
    # get random samples
    for i in range(broj_samplova[j-1]):                    
        rand_sample = np.random.choice(population,sample_size)
        mean.append(sum(rand_sample))
    
    plt.hist(mean,100)
    plt.title('N0 samples  %s' % (broj_samplova[j-1]))

    if j ==len(broj_samplova):
        break      
        





plt.suptitle('Random size %s from min %s to max %s' % (rand_size,rand_min,rand_max))
plt.savefig('clt_dist.png', transparent=False) 


plt.show()