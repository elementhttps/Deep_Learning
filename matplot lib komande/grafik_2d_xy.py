'''
Jednostavan grafik sa dve ose 
- demonstracija funkcionalnosti vise nizova u grafiku 

'''

# std plot import as plt
import matplotlib.pyplot as plt 


# x i y nizovi
x = [2,4,8,16,32,64,128]
y = [0,1,0,1,0,1,0]


# ubaci x i y nizove u grafik 
plt.plot(x,y)


# oznake na osama
plt.xlabel('x osa')
plt.ylabel('y osa')

# prikazi grafik
plt.show()


