'''

Prikaz mreza i teksta na 2d grafiku


'''

import matplotlib.pyplot as plt 


# za limite 
from matplotlib import axes as ax


import numpy as np 

#data generator
batch_data = np.random.randint(0,1000,100)


# podesavanja grafika 

#figsize uzima vrednost u incima npr 10 je 10 inca 
plt.figure(1,figsize=(25,25))
plt.title('batch data')

plt.subplot(1,2,1)
plt.grid(True)
plt.xlabel('oznaka X ose u plavoj boji', fontsize=30, color='blue')
plt.semilogx(batch_data)
plt.semilogy(batch_data)




plt.subplot(1,2,2)
plt.plot(batch_data)

# skaliranje osa 
plt.xscale('linear')
plt.yscale('log')

# limit na velicinu osa originalno 0-100
plt.xlim(0,50) 


plt.show()
