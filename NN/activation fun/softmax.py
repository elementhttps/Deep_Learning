#!/usr/bin/env python

'''
==================================
TANH FUNKCIJA U NN
==================================
- graficki prikaz
- osnovna formula


f(x)= np.exp(x) / np.sum(exp(x))


Soft max squashes the outputs of each unit to be between 0 and 1 , and divide each output with total tensor sum  


instaliraj za izvode funkcija

'''

 
import matplotlib.pyplot as plt 
# za izvode 
import autograd.numpy as np 
from autograd import elementwise_grad as egrad



def softmax_fun(x):
    '''
Funkcija za izracunavanje soft maxa  
Parameters
----------
x -koristiti np.array() ili arange 
      
Returns
-------
p - array 

p -array sadrzi brojeve izmedju 0 i 1 tzv squash
'''
    for i in x:
        p = np.exp(x) / np.sum(np.exp(x))
    return p 

   


# generate data 
x = np.array([5,4,3,2,1,0,-1,-2,-3])


# activ function plot 
print(x)


# parametri grafika 
plt.grid(True)

plt.title("Soft max funkcija")
plt.plot(softmax_fun(x))

plt.plot(x)





plt.show()
