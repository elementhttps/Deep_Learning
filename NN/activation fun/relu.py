#!/usr/bin/env python

'''
==================================
ReLU U NN
==================================
- graficki prikaz
- osnovna formula
- fast relu 
- https://www.cs.toronto.edu/%7Efritz/absps/imagenet.pdf

f(x)= (1 - exp(-2x))/(1+exp(-2x))

Limitation: 
- used within Hidden layers of a Neural Network Model.
- ReLu could result in Dead Neurons. ( fix je  Leaky ReLu)

Output layers:
- Classification should use a Softmax function 

- Regression problem it should simply use a linear function.



instaliraj za izvode funkcija

'''

 
import matplotlib.pyplot as plt 
# za izvode 
import autograd.numpy as np 
from autograd import elementwise_grad as egrad
import cProfile



def relu_fun(x):
    '''
Funkcija za izracunavanje tanh  
Parameters
----------
x -koristiti np.arrange()
      
Returns
-------
p - array 
koristiti za element wise izvod tj elementwise_grad
'''
    # p = np.maximum(x,0)
    p= x*(x>0)
    
    return p

    


# generate data 
x = np.arange(-10,10,0.1)


# activ function plot 

# parametri grafika 
plt.grid(True)


plt.subplot(121).set_title('test')
plt.title("RELU funkcija")
plt.plot(relu_fun(x))

plt.subplot(122)
plt.title("ReLU izvod")
# izvod 
plt.plot(x,egrad(relu_fun)(x))






plt.show()
