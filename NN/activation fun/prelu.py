#!/usr/bin/env python

'''
==================================
PReLU U NN tj Parametric ReLU 
==================================
- graficki prikaz
- osnovna formula
- fast relu 
- https://www.cs.toronto.edu/%7Efritz/absps/imagenet.pdf

f(x)= (1 - exp(-2x))/(1+exp(-2x))

Limitation: 
- to fix problem with dead neurons (to avoid zero gradients), aka to keep them alive use parametric and leaky ReLU (PRELU and LRELU)
- PReLU uses customizable coefficient  
- iz microsoft papira (Suppassing human level performance / Microsoft research ) - koristiti 0.25 
- leaky ReLU ima koeficijent od 0.01

Output layers:
- Classification should use a Softmax function 

- Regression problem it should simply use a linear function.





'''

 
import matplotlib.pyplot as plt 
# za izvode 
import autograd.numpy as np 
from autograd import elementwise_grad as egrad
import cProfile



def prelu_fun(x):
    '''
Funkcija za izracunavanje prelu  
Parameters
----------
x -koristiti np.arrange()
      
Returns
-------
p - array 
koristiti za element wise izvod tj elementwise_grad
'''
     # a < = 1 
    coeficient_prelu = 0.25 
    p = np.maximum(x,coeficient_prelu*x)
    
    
    return p

    


# generate data 
x = np.arange(-10,10,0.1)


# activ function plot 

# parametri grafika 
plt.grid(True)


plt.subplot(121).set_title('test')
plt.title("RELU funkcija")
plt.plot(prelu_fun(x))

plt.subplot(122)
plt.title("ReLU izvod")
# izvod 
plt.plot(x,egrad(prelu_fun)(x))






plt.show()
