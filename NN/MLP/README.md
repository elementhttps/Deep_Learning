# Multi layer pecerptron 
- kako napraviti mlp od 0 

- prvo osnovna definicija:

ovo na slici nije mlp iako imaju iste nazive, jedno sluzi za modelovanje neuronske (neuralne) mreze ,
dok drugo sadrzi neke crtane ponije 

[mlp](img/notmlp.png)

BITNA RAZLIKA :) 


![dynamic polarization](img/dynamicpolarization.png)

*Image taken from Cajal’s Law of Dynamic Polarization: Mechanism and Design (Santiago Ramón y Cajal)*

*more at https://en.wikipedia.org/wiki/Santiago_Ram%C3%B3n_y_Cajal*




# Osnovna arhitektura MLP-a 

- input layer x(i)
- non-linear hidden layer h(j) * N 
- output layer o(k)
- i,j,k jedinice ili units ( tj broj neurona u sloju) 
- N number of stacked hidden layers 

![mlp](img/mlp.png)



## Why non linearity in hidden layers?

- Neural network can model any function (not just linear functions) - [reference ](http://neuralnetworksanddeeplearning.com/chap4.html)
- Can you draw a curved line with a straight ruler? Of course, you can! However, it's more work. - 
[reference](https://mathcraft.wonderhowto.com/how-to/create-parabolic-curves-using-straight-lines-0131301/)
- If we use linear functions we can collapse out the hidden layer by an equivalently parametrized single layer perceptron
- What does make an ANN non-linear? The activation function.
- A model is linear when each term is either a constant or the product of a parameter and a predictor variable. A linear equation is constructed by adding the results for each term. This constrains the equation to just one basic form:

Response = constant + parameter * predictor + ... + parameter * predictor

<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;Y&space;=&space;b&space;o&space;&plus;&space;b_1X_1&space;&plus;&space;b_2X_2&space;&plus;&space;...&space;&plus;&space;b_nX_n" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;Y&space;=&space;b&space;o&space;&plus;&space;b_1X_1&space;&plus;&space;b_2X_2&space;&plus;&space;...&space;&plus;&space;b_nX_n" title="Y = b o + b_1X_1 + b_2X_2 + ... + b_nX_n" /></a>


- model is still linear in the parameters even though the predictor variable is squared [reference ](https://blog.minitab.com/blog/adventures-in-statistics-2/what-is-the-difference-between-linear-and-nonlinear-equations-in-regression-analysis)

![this is linear](img/mlablin.gif)

<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;Y&space;=&space;b_0&space;&plus;&space;b_1x_1&space;&plus;&space;b_2x_2^2" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;Y&space;=&space;b_0&space;&plus;&space;b_1x_1&space;&plus;&space;b_2x_2^2" title="Y = b_0 + b_1x_1 + b_2x_2^2" /></a>



- the more 'non-linear' our decision function, the more complex decisions it can make.


- if change in delta x / delta y is constant function is linear  

|X   |Y  | dx| dy| dx/dy|
| -- | --|-- |-- |--|
|1   |2  | 1 | 2 |       1/2
|2   |4  | 1 | 2 |       1/2
|3   |6  | 1 | 2 |       1/2
|4   |8  |const  |const  | const


![linear](img/linslope.png)


## What is the diffenence between MLP and linear regression ?
- jednostavno - LR nema aktivacionu funkciju tj funkcija je linearna 

## What is the difference between peceptron and neuron ? [ref](http://neuralnetworksanddeeplearning.com/chap1.html#perceptrons)
- peceptron - daje binarni izlaz 0 ili 1 
- neuron - daje vrednosti izmedju 0 i 1 

## What is the difference between MLP and Multilayer Neural Network ?
- nema razlike , iz istorijskih razloga doslo je do konfuzije [ref](http://neuralnetworksanddeeplearning.com/chap1.html#the_architecture_of_neural_networks) 
- koja konfuzija ?? pecerptron = sigmoid neuron 
- tako da kad se kaze MLP misli se na Viseslojnu Neuronsku(Neuralnu) Mrezu 
- lol 

## How many layers in a network make it deep?
- There is no right answer to this. In general, deeper networks can learn more complex functions.[ref](https://www.learnopencv.com/understanding-feedforward-neural-networks/) 




# Input layer 
- je broj ulaza u MLP 
- za razliku od drugih slojeva na ulazu nema promene vrednosti ( passive ) 
- broj ulaza se naziva i feature dimension  
- npr pikseli u crno beloj slikci dimenzija 500x500
- npr ulazne komande (WASD,space...) 


# Hidden layer 
- Hidden layer omogucava kompleksnije interakcije u neuronskoj mrezi( MLP )
- Can approximate functions much more compactly if we use deeper (vs wider) neural networks
- By having multiple hidden layers, we can compute complex functions by cascading simpler functions
- Using a single neuron we can only learn a linear decision boundary
- By adding a hidden layer, we can get rid of complex feature transformations
- Neural Network with a single hidden layer with nonlinear activation functions is considered to be a **Universal Function Approximator**
- **Universal Function Approximator** capable of learning any function


<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;h_1&space;=&space;\phi(W_1\boldsymbol{x}&space;&plus;&space;b_1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;h_1&space;=&space;\phi(W_1\boldsymbol{x}&space;&plus;&space;b_1)" title="h_1 = \phi(W_1\boldsymbol{x} + b_1)" /></a>


<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;h_2&space;=&space;\phi(W_1\boldsymbol{x}&space;&plus;&space;b_1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;h_2&space;=&space;\phi(W_2\boldsymbol{h_1}&space;&plus;&space;b_2)" title="h_1 = \phi(W_1\boldsymbol{x} + b_1)" /></a>



<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;h_3&space;=&space;\phi(W_1\boldsymbol{x}&space;&plus;&space;b_1)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;h_3&space;=&space;\phi(W_3\boldsymbol{h_2}&space;&plus;&space;b_3)" title="h_1 = \phi(W_1\boldsymbol{x} + b_1)" /></a>





## Activation functions 
- bitna razlika u odnosu na LR ( linearnu regresiju ) je i to sto sadrzi aktivacione funkcije 

- uvodjenje nelinearnosti u mrezu 



# Output layer 


# Vectorization 


# Overfitting 

- Neural networks are super flexible
- overfitting is having a model that gets 100% or 99% on the training set, but only 50% on the test data
- Even for a small number of features, deep neural networks are capable of overfitting
- Steps to reduce overfitting:
 1. Add more data  
 2. Use data augmentation  
 3. Use architectures that generalize well  
 4. Add regularization  
 5. Reduce architecture complexity.


# Regularization

- Regularization is a technique which makes slight modifications to the learning algorithm such that the model generalizes better. This in turn improves the model’s performance on the unseen data as well. [ref](https://www.analyticsvidhya.com/blog/2018/04/fundamentals-deep-learning-regularization-techniques/)
- L1 and L2 are the most common types of regularization
- lambda is the regularization parameter


# DO DEEP NETS REALLY  NEEDWEIGHT DECAY AND DROPOUT ?

- https://arxiv.org/pdf/1802.07042.pdf

# Weight Decay

- obtaining additional training data is often costly
- measure functions by their proximity to zero
- When training neural networks, it is common to use "weight decay," where after each update, the weights are multiplied by a factor slightly less than 1. This prevents the weights from growing too large, and can be seen as gradient descent on a quadratic regularization term [ref](https://metacademy.org/graphs/concepts/weight_decay_neural_networks)



# Dropout

- Used during training
- The name dropout’ arises from the notion that some neurons drop out’ of the computation for the purpose of
computing the final result
- layer “drops out” a random set of activations in that layer by setting them to zero
- Robustness through Perturbations [ref](https://arxiv.org/pdf/1703.08245.pdf) , network should be able to provide the right classification or output for a specific example even if some of the activations are dropped out


# Underfitting 

# Forward Propagation

- Forward propagation refers to the calculation and storage of intermediate variables (including outputs)
for the neural network within the models in the order from input layer to output layer


# Back Propagation

- back propagation calculates and stores the intermediate variables of an objective function related
to each layer of the neural network and the gradient of the parameters in the order of the output layer to
the input layer



# Pooling 

- amount of parameters or weights is reduced by 75%, thus lessening the computation cost
- spatial variance This property makes the network capable of detecting the object in the image without being confused by the differences in the image's textures, the distances from where they are shot, their angles, or otherwise.
- overfitting control











<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\hat{y}&space;=&space;\mbox{softmax}(W&space;\boldsymbol{x}&space;&plus;&space;b)" target="_blank"><img src="https://latex.codecogs.com/gif.latex?\inline&space;\hat{y}&space;=&space;\mbox{softmax}(W&space;\boldsymbol{x}&space;&plus;&space;b)" title="\hat{y} = \mbox{softmax}(W \boldsymbol{x} + b)" /></a>


