# OPENCV and YOLOv3 CUDA INSTALL ON LINUX 18.04 LTS 

## Installation Steps:

- [ ] [Install required packages](#install-required-packages)
- [ ] install opencv options:
    1. [Keep it simple (easy mode) ](#install-opencv-32---keep-it-simple-stupid-kiss)
    2. [pip ](#using-pip)
    3. [compiling from source (hard mode)](#compiling-opencv--401-dev-from-source-for-linux-1804-lts)
- [ ] [install YOLOv3 ](#yolov3-cuda-installation)
- [ ] ???
- [ ] profit !!11!


- Edge 9000 new model ready for spaceship integration 

![edge9000](img/edge9000.png?raw=true "Title")

### INSTALL REQUIRED PACKAGES

- Do this first 

*Check current packages* 
```
$ gcc --version && cmake --version && git --version && uname -r && lsb_release -a && nvcc --version

```
*Required packages* 
- GCC 4.4.x or later 
- CMake 2.8.7 or higher 
- Git GTK+2.x or higher, 
- Including headers (libgtk2.0-dev) pkg-config Python 2.6 or later 
and Numpy 1.5 or later with developer packages (python-dev, python-numpy) ffmpeg or libav development packages: libavcodec-dev, libavformat-dev, libswscale-dev [optional] libtbb2 libtbb-dev
- [optional] libdc1394 2.x 
- [optional] libjpeg-dev, libpng-dev, libtiff-dev, libjasper-dev, libdc1394-22-dev 
- [optional] CUDA Toolkit 6.5 or higher


- One-liner copy and paste 
```
sudo apt-get install ffmpeg libopencv-dev libgtk-3-dev python-numpy python3-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libv4l-dev libtbb-dev qtbase5-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip
```

## INSTALL OPENCV 3.2 - KEEP IT SIMPLE STUPID (KISS)


- install opencv using package manager 
```
$ sudo apt-get install libopencv-dev python3-opencv
```

## USING pip 

```
$ pip install opencv-python
```

## COMPILING OPENCV ( 4.0.1 dev) FROM SOURCE FOR LINUX 18.04 LTS

- First step

![edge9000](img/kokice.jpeg?raw=true "Title")

- and grab some liquid 

- remove conda/microconda/anaconda 
- why because it messes up python paths 
```
$ conda install anaconda-clean
```
```
$ anaconda-clean --yes
```
```
$ ~/.bash_profile
```
```
$ rm -r ~/miniconda3
```

- comment to line in ~/.bashrc:
```
#export PATH=/home/miniconda3/bin:$PATH
```
```
$ source ~/.bashrc
```

**PRE COMPILATION TASKS**

- First make folder on desktop
```
/home/SOME_USER/Desktop/Opencv/
```
```
$ cd ~/Desktop && mkdir Opencv . 
```


- Install numpy after conda removal 
```
$ pip3 install numpy
```
Successfully installed numpy-1.16.0

- Get location of numpy ( MAKE will need that info ) 
```
$ pip3 show numpy
Location: /home/BILL_GATES/.local/lib/python3.6/site-packages
```
- GIT the repos 
```
$ git clone https://github.com/opencv/opencv.git
$ git clone https://github.com/opencv/opencv_contrib.git
```
- make build folder in opencv

```
$ cd ~/opencv
```
```
$ mkdir build && cd $_
```
- folder structure will look like this 
```
/home/BILL_GATES/Desktop/Opencv/opencv
/home/BILL_GATES/Desktop/Opencv/opencv/build
```


- USE cmake-gui for configuring cmake parameters
  - remove parameters regarding python 2.* 
  - add paths to python install 
```
$ whereis python3

python3: /usr/bin/python3 /usr/bin/python3.6m /usr/bin/python3.6-config /usr/bin/python3.6 /usr/bin/python3.6m-config /usr/lib/python3 /usr/lib/python3.6 /usr/lib/python3.7 /etc/python3 /etc/python3.6 /usr/local/lib/python3.6 /usr/include/python3.6m /usr/include/python3.6 /usr/share/python3 /usr/share/man/man1/python3.1.gz

```

- enter cmake , config and generate as below 
```
$ cmake-gui
```
  
  
PYTHON3_EXECUTABLE = /usr/bin/python3
PYTHON3_INCLUDE_DIR  = /usr/include/python3.6m
PYTHON3_INCLUDE_DIR2 = <prazno za druge arhitekture >
PYTHON3_LIBRARY = /usr/lib/x86_64-linux-gnu/libpython3.6m.so
  
- Za lokaciju 
```
$ pip3 show numpy  
```
  
PYTHON3_NUMPY_INCLUDE_DIRS = /lib/python3.6/site-packages
PYTHON3_PACKAGES_PATH = /home/http27/miniconda3/lib/python3.6/site-packages
OPENCV_EXTRA_MODULES_PATH= /home/BILL_GATES/Desktop/Opencv/opencv_contrib/modules
OPENCV_ENABLE_NONFREE=ON

Or using cmake 

```
cmake -D CMAKE_BUILD_TYPE=RELEASE -D BUILD_EXAMPLES=OFF -D WITH_FFMPEG=ON -D WITH_V4L=ON -D WITH_OPENGL=ON -D WITH_CUDA=ON -D WITH_CUBLAS=ON -D WITH_CUFFT=ON -D WITH_EIGEN=ON -D CMAKE_C_COMPILER=gcc-7 -D CMAKE_CXX_COMPILER=g++-7 ..
```

**MAKE**

- Before you start make make shure that you have right gcc version 
```
$ gcc --version
```
- Version not 7.x ? Try 
```
sudo apt install g++-6 gcc-6
```
And update alternatives 
```
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 1 --slave /usr/bin/g++ g++ /usr/bin/g++-7
```
And switch to gcc 7 on prot

```
$ sudo update-alternatives --config gcc

```
No gcc problems - please continue  


To see number of cores enter nproc or see in htop/top/atop 
```
$ nproc
```
- depending on the number of cores after -j flag input desired number of cores ( more the better )
NOTE: are you still in build folder ? 

```
$ time make -j10
```

- after 5-10-30 min (depending on core numbers ) and no compilation errors (or good luck) 
- install compiled 
```
$ sudo make install
```

```
$ opencv_version
```

**IMPORTANT** 
- now import in your .py file 
- import cv2
- we installed opencv for **python3** NOT for python2.x version 
- if you type 
```
$ python img_display.py 
```
- it will trow classic opencv an error 
```
  File "img_display.py", line 13, in <module>
    import cv2
ImportError: No module named cv2

```
- **USE python3**

```
$ python3 img_display.py
```

```
$ bonus line 

```

## YOLOv3 CUDA INSTALLATION

```
$ git clone https://github.com/AlexeyAB/darknet
```
```
$ cd darknet
```

```
$ vi Makefile
```
set: 
for CUDA install 
*GPU=1*
and OPENCV install
*OPENCV=1*

**MAKE**

NOTE: modify -j<int> flag 
check n0 cores 
    
```
$ nproc    
```

```
$ time make -j10
```

If all goes well test it using:
```
$ ./darknet imtest data/eagle.jpg
```

You get lots of stacked eagles 

![orlovi](img/eagles.png)

**BUT WAIT!!!**
**there is more**

![tim](img/tim.jpg?raw=true "Title")


Get the ANN weights 
```
wget https://pjreddie.com/media/files/yolov3.weights
```

Start some grafic monitoring tool 

```
$ nvtop
```


![funsw](img/jediorsith.jpg?raw=true "Title")


![funt](img/errorlol.jpg?raw=true "Title")

*Classification error ?*

```
./darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg
```


crtl+C to close 

**NOTE:CUDA Error: out of memory ERROR**
reference https://github.com/AlexeyAB/darknet#how-to-improve-object-detection
to fix this error in yolov3.cfg change subdivisions to 32 
```
$ vi ./cfg/yolov3.cfg

and set 
from
subdivisions=16

set
subdivisions=32
```

Save output video

```
./darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights -thresh 0.25 catc.gif -out_filename res.avi
```
**NOTE:No output video?**

Check these one: 

https://github.com/AlexeyAB/darknet/issues/1998

https://github.com/AlexeyAB/darknet/issues/1713

https://github.com/AlexeyAB/darknet/issues/449

NOTE to self : dont clone wrong repository lol



![concat](img/ccat.gif?raw=true "Title")




