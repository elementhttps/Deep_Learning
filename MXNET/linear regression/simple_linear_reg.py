'''
==================================
Linearna regresija  algoritam 
==================================
- osnovna formula 
- ...

----------------------------------------------------
Linearna regresija
----------------------------------------------------
- formula:
...

----------------------------------------------------
Simple 6 PART process
----------------------------------------------------
1. Import what is needed from framework 
2. Get / clean /transform data 
3. Setup/Define model 
4. Train model 
5. Evaluate / optimize  
6. Make prediction



'''

'''
----------------------------------------------------
1. Uzmi sta ti treba iz framework-a / i konfiguracija 
----------------------------------------------------
'''
import mxnet as mx
import numpy as np

# Fix the random seed
mx.random.seed(42)

import logging
logging.getLogger().setLevel(logging.DEBUG)
'''
----------------------------------------------------
2. Get / clean /transform data 
----------------------------------------------------
'''
#Training data
train_data = np.random.uniform(0, 1, [100, 2])
train_label = np.array([train_data[i][0] + 2 * train_data[i][1] for i in range(100)])
batch_size = 1

#Evaluation Data
eval_data = np.array([[7,2],[6,10],[12,2]])
eval_label = np.array([11,26,16])

train_iter = mx.io.NDArrayIter(train_data, train_label, batch_size, shuffle=True, label_name='lin_reg_label')
eval_iter = mx.io.NDArrayIter(eval_data, eval_label, batch_size, shuffle=False, label_name='lin_reg_label')
'''
----------------------------------------------------
3. Setup / Define model 
----------------------------------------------------
'''
X = mx.sym.Variable('data')
Y = mx.symbol.Variable('lin_reg_label')
fully_connected_layer  = mx.sym.FullyConnected(data=X, name='fc1', num_hidden = 1)
lro = mx.sym.LinearRegressionOutput(data=fully_connected_layer, label=Y, name="lro")

model = mx.mod.Module(
    symbol = lro ,
    data_names=['data'],
    label_names = ['lin_reg_label']# network structure
)
mx.viz.plot_network(symbol=lro, node_attrs={"shape":"oval","fixedsize":"false"})
'''
----------------------------------------------------
4. Train model 
----------------------------------------------------
'''
model.fit(train_iter, eval_iter,
            optimizer_params={'learning_rate':0.01, 'momentum': 0.9},
            num_epoch=20,
            eval_metric='mse',
            batch_end_callback = mx.callback.Speedometer(batch_size, 2))


'''
----------------------------------------------------
5. Evaluate   
----------------------------------------------------
'''
model.predict(eval_iter).asnumpy()
'''
----------------------------------------------------
6. Make prediction  
----------------------------------------------------
'''